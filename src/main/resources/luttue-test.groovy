import io.lettuce.core.RedisClient

def password = ''
def redisClient = RedisClient.create("redis://$password@localhost:6379/0")
def connection = redisClient.connect()
def syncCommands = connection.sync()

syncCommands.set("key", "Hello, Redis!")

connection.close()
redisClient.shutdown()